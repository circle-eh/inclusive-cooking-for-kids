{% if page.meta.author %}
** Author: ** {{ page.meta.author }}
{% endif %}


{{ page.meta.description }}

| Serves        | Prep Time       | Cooking Time   | Allergens | Kid Friendly |
| :-----------: | :-------------: | :------------: | --------- | :-----------: |
| {{ page.meta.serves }}  | {{ page.meta.preptime }} {{ page.meta.preptime_units }} | {{ page.meta.cooktime}} {{ page.meta.cooktime_units }} | <ul>{% for allergen in page.meta.allergens %}<li>{{ allergen }}</li> {% endfor %}</ul> | {{ page.meta.kidfriendly }} |

## Ingredients

{% for ingredient in page.meta.ingredients %}
  * {{ ingredient.quantity }} {{ ingredient.unit }} {{ ingredient.ingredient }}
{% endfor %}

## Instructions

{{ page.meta.instructions }}
