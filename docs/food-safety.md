---
date: 2019-01-17 14:30:00 -0400
title: Food Safety
---


# Food Safety

Food safety requirements are different from place to place, and your legal
requirements may differ from what is suggested here.  This document is meant
as a best-practice, and is not legally binding, or suitable for commercial food
preparation.

If you are unsure, please check with your local jurisdiction on local food
safety requirements.

## Basic Guidelines

By only serving plant-based foods, without any animal ingredients, we already
are at significantly reduced risk from food-born pathogens (most commonly
bacteria such as e. coli or salmonella).

### Food Storage

If your food will be prepared and served within 3 hours, you are within the
safety window where bacteria will not have had enough time to grow to levels
that could be dangerous for human consumption.

Bacteria multiplies the fastest at temperatures between 4°C and 60°C, so if you
plan on preparing the food and serving it more than 3 hours after it has been
cooked, you need to ensure that it is either kept refrigerated or frozen, or
heated at above 60°C.

Always use clean food-safe containers for food storage and service.

### Hygiene

Practice basic hygiene before preparing, cooking, or serving food. That means:

  * Wash your hands and wrists with warm water and soap.
  * Do not touch your mouth or face while preparing, cooking, or serving food.
  * If you take a break, particularly if you go to the bathroom, wash your hands
    with warm soapy water again before handling food.
  * If you are sick, consider not handling food that will be served to others.
  * When necessary, wear a hair net or hat, and a beard net.

### Allergies and Cross-contamination

Food-based allergies can be life threatening and need to be taken seriously.

When preparing food that may be consumed by someone with allergies, begin with
a thorough cleaning of the surfaces (counters, sinks, cutting boards, stove
tops) and equipment (knives, serving utensils, pots, pans, etc) with a generous
amount of soap and hot water.

DO the food preparation for allergy-friendly meals before any food preparation
for any meals that may include allergens. For example, clean your kitchen, make
the food that will be shared, and when it is properly stored for a later
service, make yourself a PB&J sandwich.

DO label food with the ingredient list, in case someone has not previously
disclosed their allergy.  This also helps people who may choose not to eat
certain foods for reasons unrelated to allergies to make informed decisions.

DO NOT store foods containing allergens next to the allergen-free foods in the
fridge or freezer.

DO NOT serve foods containing allergens at the same meals you know there will
be someone with those allergies.
