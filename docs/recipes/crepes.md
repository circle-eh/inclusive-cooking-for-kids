---
title: Crêpes
disqus: 'inclusive-cooking'
language: en
type: recipe
date: 2019-01-21T18:25:48.986Z
description: >-
  Living in Québec, crêpes are a very common breakfast food for large
  gatherings.  This recipe is easy to prepare if you have a blender, and cooks
  up in no time.
author: Chris Poupart
serves: '20'
preptime: 5
preptime_units: minutes
cooktime: 40
cooktime_units: minutes
category: Breakfast
ingredients:
  - ingredient: ripe bananas
    quantity: 8
    unit: []
  - ingredient: flour
    quantity: 8
    unit: cup(s)
  - ingredient: vanilla soy milk
    quantity: 2
    unit: litre(s)
  - ingredient: granulated sugar
    quantity: 2
    unit: cup(s)
  - ingredient: dairy-free margarine
    quantity: 2
    unit: cup(s)
instructions: !
  1. Preheat your crêpes pan, or a non-stick skillet. Thinner aluminium pans
     will heat up well on medium-low. Cast-iron or other thicker pans can be
     heated up on medium.  Add some oil or margarine if needed to keep the
     crêpes from sticking.


  1. Place the bananas, soy milk, margarine, and sugar into a blender and blend
     until smooth (work in batches if your blender is not large enough).

  1. Add the flour and process until just combined. If you don't have room in
     your blender to add the flower, then pour the combined wet ingredients into
     a large bowl, add the flour, and stir until just combined.

  1. Pour 1/4 cup of batter into your pan and spread it out.  Cook until the top
     of the crepe is dry, and then turn it over to lightly brown the top.

  1. Remove from heat and serve.
     **Note:** The crêpes can be cooled and stored
     covered for later service. Prepare them for serving by heating them briefly
     (15 seconds per side) on a hot pan.

allergens:
  - Wheat
  - Soy

kidfriendly: true
---
{% include "recipe_template.md" %}
{% include "glossary.md" %}
