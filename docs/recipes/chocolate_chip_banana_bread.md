---
title: Chocolate Chip Banana Bread
disqus: 'inclusive-cooking'
language: en
date: 2019-01-19T12:59:09.060Z
type: recipe
description: Who doesn't love a good chocolate-chip banana bread?
author: Chris Poupart
serves: 12
preptime: 15
preptime_unit: minutes
cooktime: 60
cooktime_unit: minutes
category: Snacks and Desserts
ingredients:
  - ingredient: brown sugar
    quantity: 0.5
    unit: cup(s)
  - ingredient: white sugar
    quantity: 0.5
    unit: cup(s)
  - ingredient: semi-sweet chocolate chips
    quantity: 1
    unit: cup(s)
  - ingredient: margarine, at room temperature
    quantity: 1
    unit: cup(s)
  - ingredient: very ripe bananas, mashed well
    quantity: 3
    unit: ""
  - ingredient: flour
    quantity: 2
    unit: cup(s)
  - ingredient: baking soda
    quantity: 0.5
    unit: teaspoon(s)
  - ingredient: vanilla soy milk
    quantity: 0.25
    unit: cup(s)
  - ingredient: apple cider vinegar
    quantity: 1
    unit: teaspoon(s)
  - ingredient: vanilla
    quantity: 1
    unit: teaspoon(s)
  - ingredient: salt (optional)
    quantity: 0.5
    unit: teaspoon(s)
instructions: |
  1. Preheat oven to 350 F (180 C). Coat a loaf pan with cooking spray or
     lightly coat with margarine.
  1. Sift together your flour, baking soda, and salt.
  1. In a separate bowl, cream together the bananas, margarine, and sugar.  Add
     the soy milk and vinegar mixture and combine.
  1. Add your dry ingredients into the wet and stir to combine.
  1. Pour the batter into the pan and bake for around 1 hour, until a knife
     inserted into the middle comes out clean.

allergens:
  - soy
  - wheat
kidfriendly: true
---

{% include "recipe_template.md" %}
{% include "glossary.md" %}