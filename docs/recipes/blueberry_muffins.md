---
title: Blueberry Muffins
disqus: 'inclusive-cooking'
language: en
type: recipe
description: >-
  This is a very basic recipe that can be modified as needed. Use other fruits,
  add some spices. Swap the fruit for chocolate chips. The cooking time is for
  mini-muffins, which seem like a better size for snacks and kids, as they lead
  to less waste.
author: Chris Poupart
serves: '24'
preptime: 5
preptime_units: minutes
cooktime: 20
cooktime_units: minutes
category: Snacks and Desserts
ingredients:
  - ingredient: whole wheat pastry flour
    quantity: 3
    unit: cup(s)
  - ingredient: brown sugar
    quantity: 1
    unit: cup(s)
  - ingredient: baking powder
    quantity: 4
    unit: tsp(s)
  - ingredient: vanilla soy milk
    quantity: 1.5
    unit: cup(s)
  - ingredient: vegetable oil
    quantity: 0.5
    unit: cup(s)
  - ingredient: frozen wild blueberries
    quantity: 2
    unit: cup(s)
instructions: |
  1. Preheat oven to 400 F and grease a small muffin tin (a muffin tin that
     makes 24 mini muffins).
  1. In a mixing bowl, combine flour, sugar, and baking powder.
  1. Stir in vanilla soy milk, and oil.
  1. Fold in blueberries.
  1. Pour into equal portions in the muffin tin, and bake for 20 to 22 minutes,
     until a knife inserted into the middle of a muffin comes out clean.

allergens:
  - Wheat
  - Soy
kidfriendly: true
---

{% include "recipe_template.md" %}
{% include "glossary.md" %}
