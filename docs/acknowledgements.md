---
title: Acknowledgements
description: 'Thanks-yous, contributors, and list of sources for this book.'
---
## Contributors

* Chris Poupart [:fontawesome-brands-github:](https://github.com/chrispoupart) [:fontawesome-brands-gitlab:](https://gitlab.com/chrispoupart)
